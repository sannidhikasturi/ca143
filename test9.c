#include <stdio.h>
#include <stdlib.h>

int* input()
{
    int *x = (int*)malloc(sizeof(int));

    printf("Enter a number.\n");
    scanf("%d", x);

    return x;
}

void swap(int *x, int *y)
{
    int temp;

    temp = *x;
    *x = *y;
    *y = temp;
}

void print(int *x, int *y)
{
    printf("X = %d\nY = %d", *x, *y);
}

void process()
{
    int *x, *y;

    x = input();
    y = input();

    printf("Before swapping : \n");
    print(x, y);

    swap(x, y);

    printf("\nAfter swapping : \n");
    print(x, y);
}

int main(void)
{
    process();
}

