#include<stdio.h>
int main()                                                           
{
    int  a, b, *p1, *p2, addition,subtraction,multiplication,rem;                               
    printf("Enter integer 1:\n");
    scanf("%d",&a);
    printf("Enter integer 2:\n");
    scanf("%d",&b);
    p1 = &a;                                                   
    p2 = &b;                                                     
    addition  =  *p1 + *p2;                                        
    subtraction =  *p1 - *p2;   
    multiplication=*p1 * *p2;
    rem=*p1 % *p2;
    printf("Addition of the two integers = %d",addition);
    printf("Subtraction of the two integers = %d",subtraction);
    printf("Multiplication of the two integers = %d",multiplication);
    printf("Remainder of the two integers = %d",rem);
    return 0;

}                                                                
