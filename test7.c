#include <stdio.h>
#include <stdlib.h>

static char *input()
{
    char *str = malloc(30 + 1);
    
    printf("Enter a string.\n");
    gets(str);
    
    return str;
}

void concatenate()
{
    char *str1, *str2;
    static int i = 0, j = 0;
    
    str1 = input();
    str2 = input();
    
    char str[60];
    
    while (str1[i] != '\0')
    {
        str[i] = str1[i];
        i++;
    }
    
    while (str2[j] != '\0')
    {
        str[i] = str2[j];
        j++;
        i++;
    }
    
    str[i] = '\0';
    
    printf("Concatenated = %s", str);
}

int main(void)
{
    concatenate();
}
