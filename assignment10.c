#include<stdio.h>
int main()
{
    int array[10],pos=-1,value;
    printf("Enter the elements of the array:\n");
    for(int i=0;i<10;i++)
        scanf("%d",&array[i]);
    printf("Enter the element to be searched:\n");
    scanf("%d",value);
    for(int i=0;i<10;i++)
    {
        if (array[i]==value)
            pos=i+1;
    }
    if(pos==-1)
        printf("Value not found!\n");
    else
        printf("Value found at %d",pos);
    return 0;
}