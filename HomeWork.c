//1.Write a program to print the position of the smallest of n numbers using arrays.

#include <stdio.h>
int main()
{
	int i,n,arr[20];
	int small_pos=0;
	printf("Enter the number of elements in the array:\n");
	scanf("%d",&n);
	printf("Enter the values:\n");
	for(i=0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	for(i=0;i<n;i++)
		if(arr[i]<small)
			small _pos = i;
	printf("The position of the smallest number in the array is : %d" ,small_pos);
	return 0;
}

//2.Write a program to find the second largest number and also display its position.

#include<stdio.h>
int main()
{
	int i,j,n,arr[20],temp;
	printf ("Enter the number of elements in the array:\n");
	scanf("%d",&n);
	printf("Enter the values:\n");
	for(i=0;i<n;i++)
		scanf("%d",&arr[i]);
	for (i=0;i<n-1;i++)
	{
		for (j=0;j<n-i-1;j++)
		{
			if (arr[j]>arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
	printf("The second largest number in the array = %d",arr[n-1]);
	printf("The position is = %d",n-1);
	return 0;
}

//3.Write a program to display the square and cube of each number stored in an array. Store square and cubes in 2 different arrays.

#include<stdio.h>
int main()
{
	int array[20],square[20],cube[20],i,n;
	printf("Enter the size of the array:\n");
	scanf("%d",&n);
	printf("Enter the elements:/");
	for(i=0;i<n;i++)
	{
		scanf("%d",&array[i]);
		square[i]=array[i]*array[i];
		cube[i]=array[i]*array[i]*array[i];
	}
	printf("The squares of the elemnts are:\n");
	for(i=0;i<n;i++)
		printf("%d\n",square[i]);
	printf("The cubes of the elements are:\n");
	for(i=0;i<n;i++)
		printf("%d\n",cube[i]);
	return 0;
}

//4.Write a program to find the number of duplicate elemnts in an array.

#include<stdio.h>
int main()
{
	int c=0,n,i,j,array[20];
	printf("Enter the size of the array:\n");
	scanf("%d",&n);
	printf("Enter the elements:/");
	for(i=0;i<n;i++)
		scanf("%d",&array[i]);
	for(i=0;i<n; i++)
		for(j=i+1;j<n;j++)
		{
			if(arr[i]==arr[j])
				c++;
		}
	printf("The number of duplicate elements in the given array = %d",c);
	return 0;
}

