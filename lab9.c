#include<stdio.h>
int main()
{
    struct student
    {
        char name[20];
        char dept[20];
        char sec;
        float fees,marks;
        int rollno;
    }stud1,stud2;
    printf("Enter the name of the student 1:\n");
    scanf("%s",stud1.name);
    printf("Enter the department:\n");
    scanf("%s",stud1.dept);
    printf("Enter the fees:\n");
    scanf("%f",&stud1.fees);
    printf("Enter the section:\n");
    scanf("%c",&stud1.sec);
    printf("Enter the roll number:\n");
    scanf("%d",&stud1.rollno);
    printf("Enter the total marks scored:\n");
    scanf("%f",&stud1.marks);
    printf("Enter the name of the student 2:\n");
    scanf("%s",stud2.name);
    printf("Enter the department:\n");
    scanf("%s",stud2.dept);
    printf("Enter the fees:\n");
    scanf("%f",&stud2.fees);
    printf("Enter the section:\n");
    scanf("%c",&stud2.sec);
    printf("Enter the roll number:\n");
    scanf("%d",&stud2.rollno);
    printf("Enter the total marks scored:\n");
    scanf("%f",&stud2.marks);
    printf("Highest marks has been scored by:\n");
    if(stud1.marks>stud2.marks)
        printf("Name=%s\nDepartment=%s\nSection=%c\nRoll Number=%d\nFees Paid by Student=%f\nTotal marks scored=%f\n",stud1.name,stud1.dept,stud1.sec,stud1.rollno,stud1.fees,stud1.marks);
    else  
        printf("Name=%s\nDepartment=%s\nSection=%c\nRoll Number=%d\nFees Paid by Student=%f\nTotal marks scored=%f\n",stud2.name,stud2.dept,stud2.sec,stud2.rollno,stud2.fees,stud2.marks);
    return 0;
}